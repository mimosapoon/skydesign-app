import React from 'react';
import { Header, Main } from './components';
import './styles/App.scss';

function App() {
  return (
    <div className="App">
      <Header/>
      <Main />
    </div>
  );
}

export default App;
