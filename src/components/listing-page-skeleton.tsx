import styles from '../styles/modules/ListingPage.module.scss';

interface ListingPageSkeletonProps {
    count: number
}

const ListingPageSkeleton = ({count}: ListingPageSkeletonProps) => {
    return (
        <div className={`${styles.listingContainer} ${styles.listingContainerSkeleton}`}>
            {new Array(count).fill(0, 0, count).map((value, index) => (
                <div key={`skeleton-${index}`} className={styles.listingItem}>
                    <div className={`${styles.thumbnail} ${styles.skeleton}`}></div>
                    <div className={`${styles.title} ${styles.skeleton}`}></div>
                    <div className={`${styles.date} ${styles.skeleton}`}></div>
                    <div className={`${styles.vote_average} ${styles.skeleton}`}></div>
                    <div className={`${styles.overview} ${styles.skeleton}`}></div>
                    <div className={`${styles.popularity} ${styles.skeleton}`}></div>
                </div>
            ))}
        </div>
    )
}

export default ListingPageSkeleton


