import { memo } from "react";
import ReactPaginate from 'react-paginate';
import { DiscoverMovie } from "../api-utils/types/api";
import { getImagesUrl } from "../utils/utils";
import { ArrowIcon, StarIcon} from "./icons";
import styles from '../styles/modules/ListingPage.module.scss';
import paginationStyles from '../styles/modules/Pagination.module.scss';

interface ListingPageProps{
    listings: DiscoverMovie,
    updateQueryObject: (newObject: {[key:string]:string}) => void
}

const ListingPage = memo(({listings, updateQueryObject}: ListingPageProps) => {
    const { total_pages, results } = listings;

    const handlePageClick = (event: {selected: number} ) => {
        updateQueryObject({
            page: (event.selected + 1).toString()
        })
    }

    return (
        <div className={styles.listingContainer}>
            {
                results.map( ({title, poster_path, overview, popularity, release_date, vote_count, vote_average}, index) => {
                    return (
                        <div key={`listing-${index}`} className={styles.listingItem}>
                            {
                                poster_path && (
                                    <div className={styles.thumbnail} style={{backgroundImage: `url(${getImagesUrl({width: 235, height: 235, profileUrl:poster_path})})`}}></div>
                                )
                            }
                            <div className={styles.title}><h1>{title}</h1></div>
                            <div className={styles.date}>{release_date}</div>
                            <div className={styles.vote_average}><StarIcon/> <span>{vote_average}</span></div>
                            <div className={styles.overview}><p>{overview}</p></div>
                            <div className={styles.popularity}>Popularity: <span>{popularity}</span></div>
                        </div>
                    )
                })
            }
            <ReactPaginate
                breakLabel="..."
                nextLabel={<span className={paginationStyles.next}><ArrowIcon /></span>}
                onPageChange={handlePageClick}
                pageCount={total_pages}
                previousLabel={<span className={paginationStyles.previous}><ArrowIcon /></span>}
                containerClassName={paginationStyles.pagination}
                pageClassName={paginationStyles.page}
                activeClassName={paginationStyles.pageActive}
                disabledClassName={paginationStyles.disabled}
                renderOnZeroPageCount={ () => {}}
            />
        </div>
    )
})

export default ListingPage


