import * as THREE from 'three'
import React, { useRef, useState} from 'react'
import { useFrame, ThreeElements, useLoader } from '@react-three/fiber'

interface PlaneProps  {
    imageUrl: string,
    positionX:number,
    rotationY: number,
    positionZ: number,
    onHover: () => void,
    onMouseOut: () => void
}

function Plane(props: ThreeElements['mesh'] & PlaneProps) {
    const { imageUrl, rotationY, positionZ, positionX, onHover, onMouseOut } = props;
    const ref = useRef<THREE.Mesh>(null!)

    const [hovered, hover] = useState(false)
    const [clicked, click] = useState(false)
    const destMoveZ = 1;
    let [rotateY, setRotateY] = useState(0)
    let [moveZ, setMoveZ] = useState(0)
    let [moveX, setMoveX] = useState(0)
    let [cameraMoveX, setCameraMoveX] = useState(0)

    const degreeToRadian = (degree: number) => degree * Math.PI / 180;

    useFrame((state) => {
        if (hovered) {
            setMoveZ(() => (destMoveZ - ref.current.position.z) * 0.05);
            if(moveZ >= 0.0001) {
                ref.current.position.z += moveZ;
            }

            setRotateY(() => (0 - ref.current.rotation.y) * 0.05);
            ref.current.rotation.y += rotateY;

            const objectX = ref.current.getWorldPosition(ref.current.position);
            setCameraMoveX( () => (objectX.x - state.camera.position.x) * 0.01);
            state.camera.position.x += cameraMoveX;

        }else {
            setMoveZ(() => (positionZ - ref.current.position.z) * 0.05);
            if(moveZ <= -0.0001) {
                ref.current.position.z += moveZ;
            }

            setRotateY(() => (rotationY - ref.current.rotation.y) * 0.05);
            ref.current.rotation.y += rotateY;


            setMoveX(() => (positionX - ref.current.position.x) * 0.05);
            ref.current.position.x += moveX;
        }
    });

    const texture = useLoader(THREE.TextureLoader, imageUrl)
    return (
        <mesh
            {...props}
            ref={ref}
            scale={clicked ? 5 : 5}
            onClick={(event) => click(!clicked)}
            onPointerOver={(event) => {
                event.stopPropagation()
                hover(true);
                onHover();
            }}
            onPointerOut={(event) => {
                event.stopPropagation()
                hover(false)
                onMouseOut()
            }}
            rotation={[0,rotationY, rotationY * 0.09]}
        >
            <planeGeometry args={[1, 1, 1]}/>
            <meshBasicMaterial attach="material" map={texture} transparent={true} />
         </mesh>
    )
}

export default Plane

