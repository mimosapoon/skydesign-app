import logo from './../images/logo.png';

const Header = () => {
    return (
        <header className="header">
            <div className='panel container-fluid'>
                <img src={logo} className="logo" alt="logo" />
            </div>
        </header>
    )
}

export default Header