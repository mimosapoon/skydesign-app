import React from 'react';
import { render } from '@testing-library/react';
import ListingPage from './listing-page';
import discoverMovieMock from "../mocks/discover-movie";

const setup = () => {
    const props = {
        listings: discoverMovieMock,
        updateQueryObject: () => {},
    }
    const utils = render(<ListingPage {...props} />)
    return {
        ...utils,
    }
}

test('It should render listing page', () => {
    const { container } = setup()
    expect(container.getElementsByClassName('listingItem').length).toBe(discoverMovieMock.results.length)
});