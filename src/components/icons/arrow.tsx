const Arrow = () => {
    return (
        <svg viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg">
            <path d="M19.23,25.75c-.3,0-.6-.11-.84-.33L7.93,15.93c-.26-.24-.41-.57-.41-.93s.15-.69,.41-.93L18.39,4.57c.51-.46,1.3-.43,1.77,.08,.46,.51,.43,1.3-.08,1.77L10.63,15l9.44,8.58c.51,.46,.55,1.25,.08,1.77-.25,.27-.58,.41-.92,.41Z"/>
        </svg>
    )
}

export default Arrow