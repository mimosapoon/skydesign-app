import React, {useState} from "react";
import { Canvas } from '@react-three/fiber'
import { ThreePlane} from "./index";
import styles from "../styles/modules/ListingPage.module.scss";
import {StarIcon} from "./icons";
import {DiscoverMovie} from "../api-utils/types/api";
import {getImagesUrl} from "../utils/utils";

interface ThreeMainProps{
    listings: DiscoverMovie
}

export default function ThreeMain({listings}: ThreeMainProps) {
    const [hoverId, setHoverId] = useState<number>(-1);
    const onHoverHandler = (index: number) => {
        setHoverId(index);
    }
    const onMouseOutHandler = () => {
        setHoverId(-1);
    }

    return (
        <div className={styles.threeContainer}>
            <Canvas camera={{ position: [0, 0, 6] }}>
                <ambientLight />
                <pointLight position={[10, 10, 10]} />
                {
                    listings && listings.results.map( ({title, poster_path, overview, popularity, release_date, vote_count, vote_average}, index) => {
                        const remainder = Math.floor((index + 1) % 2);
                        const quotient = Math.floor((index + 1) / 2);
                        let posX = 0;
                        let rotateY = 0;
                        let posZ = 0.5;
                        posX = index !== 0 ? remainder === 0 ? quotient * 2  : -quotient* 2 : 0;
                        rotateY = index !== 0 ? remainder === 0 ? -quotient * 0.06  : quotient  * 0.06: 0;
                        posZ = -quotient * 0.03;
                        return (
                            <ThreePlane position={[0, 0, 0]} imageUrl={getImagesUrl({width: 235, height: 235, profileUrl: poster_path, isLocal: true})} rotationY={rotateY} positionZ={posZ} positionX={posX} onHover={ () => onHoverHandler(index)} onMouseOut={ () => onMouseOutHandler} />
                        )
                    })
                }
            </Canvas>
            {
                hoverId > -1 && (
                    <div className="panel container-fluid">
                        <div className={styles.threeLists}>
                            <div className={styles.vote_average}><StarIcon/><span> {listings.results[hoverId].vote_average}</span></div>
                            <div className={styles.title}><h1>{listings.results[hoverId].title}</h1></div>
                            <div className={styles.date}> {listings.results[hoverId].release_date}</div>
                            <div className={styles.overview}> {listings.results[hoverId].overview}</div>
                        </div>
                    </div>
                )
            }
        </div>
    )
}