import { memo, useEffect, useState, useCallback, ChangeEvent, MouseEvent } from 'react'
import { FilterTag } from "../index";
import { isLatinWords, getImagesUrl, filterSearchResultsByType } from '../../../utils/utils';
import { requestItem } from '../../../api-utils/request';
import { SearchPeople } from "../../../api-utils/types/api";
import { AvatarIcon, SearchIcon, WarningIcon} from "../../icons";
import styles from '../../../styles/modules/Searchbar.module.scss';

interface SearchBarProps {
    placeHolder?: string,
    searchType: string,
    maxSelectedOption: number,
    getSelectedOptionIds: (selectedOptions: string[]) => void
}

const SearchBar = memo (({placeHolder, searchType, maxSelectedOption, getSelectedOptionIds}: SearchBarProps) => {
    const [searchResult, setSearchResult] = useState<SearchPeople['results']>()
    const [searchString, setSearchString] = useState<string>('');
    const [showDropdown, setShowDropdown] = useState(false);
    const [selectedOptions, setSelectedOptions] = useState<{[key:string]:string}[]>([]);
    const [warningMessage, setWarningMessage] = useState<string | null>(null);

    useEffect( () => {
        if(selectedOptions.length > 0) {
            getSelectedOptionIds(selectedOptions.map( ({id}) => id));
        }
    },[selectedOptions]);

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const input = event.target.value
        const isLatin = isLatinWords(input);

        setSearchString(input)

        if(isLatin && input.length > 1) {
            const encodedKeyword = encodeURIComponent(input)
            requestItem({searchType, keyword: encodedKeyword})
                .then( (data) => {
                    if(data.status_code) {
                        setWarningMessage('Request failed, please try again.');
                        return;
                    }
                    if(data.results && data.results.length > 0) {
                        setSearchResult(filterSearchResultsByType({type: searchType, results: data.results}))
                        setWarningMessage(null);
                        setShowDropdown(true)
                    }else {
                        setSearchResult([])
                    }
                })
                .catch((error) => {
                    console.log(error);
                    setWarningMessage('Request failed, please try again.')
                })
        }else {
            setSearchResult([])
            setShowDropdown(false)
        }
    }

    const isOptionSelected = useCallback( (optionId: string) => selectedOptions.find( ({id:selectedId}) => optionId === selectedId), [selectedOptions])

    const onSelectedOption = ({event, name, selectedId, profile_path}: {event: MouseEvent<HTMLLIElement>, name: string, selectedId: string, profile_path: string}) => {
        event.stopPropagation();
        event.preventDefault();

        setSearchString('');
        setShowDropdown(false);

        if(!isOptionSelected(selectedId) && selectedOptions.length < maxSelectedOption) {
            setSelectedOptions([...selectedOptions, {
                name,
                id: selectedId,
                profile_path
            }]);
        }
    }

    useEffect( () => {
        if(selectedOptions.length === maxSelectedOption) {
            setWarningMessage('Please choose a maximum of two actors');
        }else {
            setWarningMessage('');
        }
    }, [searchResult, selectedOptions, maxSelectedOption])

    const onRemoveOption = (filterTagId: string) => setSelectedOptions(selectedOptions.filter( ({id}) => id !== filterTagId));

    return (
        <div className={styles.searchBarContainer}>
            <div className={styles.searchBar}>
                <div className={styles.inputBox}>
                    <i className="searchIcon"><SearchIcon /></i>
                    <input
                        type="text"
                        aria-label="search-input"
                        placeholder={placeHolder}
                        onChange={handleChange}
                        value={searchString}
                        onBlur={ (event) => {
                            event.preventDefault()
                            event.target.blur()
                            setShowDropdown(false)
                        }}
                        autoFocus
                        onClick={ (e) => {
                            // setShowDropdown(true);
                        }}
                    />
                </div>
            </div>
            {
                showDropdown &&
                (
                    <ul className={styles.dropdown} aria-label="search-dropdown">
                        {
                            warningMessage && <li className={styles.warningMessage}><WarningIcon/>{warningMessage}</li>
                        }
                        {
                            searchResult && searchResult.length > 0
                                ? searchResult.map( ({name, id, profile_path}, i) => (
                                    <li key={`search-result-${i}`}
                                        className={`${isOptionSelected(id) || selectedOptions.length === maxSelectedOption? styles.optionDisabled: ''}`}
                                        onMouseDown={(e) => e.preventDefault()}
                                        onClick={(event) => onSelectedOption({event, selectedId: id, name, profile_path})}
                                    >
                                        <div className={styles.image} style={profile_path? {backgroundImage: `url(${getImagesUrl({width: 235, height: 235, profileUrl: profile_path})})`}: {}}>
                                            { !profile_path && <AvatarIcon/>}
                                        </div>
                                        <div className={styles.title}>{name}</div>
                                    </li>
                                ))
                                : !warningMessage && <li className="no-search">Search actor's name</li>
                        }
                    </ul>
                )
            }
            {
                selectedOptions.length > 0 && (
                    <div className={styles.tags} aria-label="search-tag"> {
                        selectedOptions.map( ({name, id, profile_path} , index) => (
                            <FilterTag key={`tag-${index}`} title={name} imageUrl={profile_path} onSelectTag={() => onRemoveOption(id)}/>
                        ))
                    }
                    </div>
                )
            }
        </div>
    );
})

export default SearchBar;
