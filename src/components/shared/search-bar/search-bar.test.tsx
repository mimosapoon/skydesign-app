import React from 'react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import SearchBar from './search-bar';
import searchPeopleMock from "../../../mocks/search-people";

jest.mock('query-string', () => ({}))

const myServer = setupServer(
    rest.get('https://api.themoviedb.org/3/search/person?api_key=74861c76b16db29a7f02f0f50dd33ce5&language=en-US&query=al&includeAdult=false', (req, res, ctx) => {
        return res(ctx.json(searchPeopleMock))
    })
)

beforeAll(() => myServer.listen())
afterEach(() => myServer.resetHandlers())
afterAll(() => myServer.close())

const setup = () => {
    const props = {
        searchType: 'Acting',
        maxSelectedOption: 2,
        getSelectedOptionIds: () => {},
    }
    const utils = render(<SearchBar {...props} />)
    const input = screen.getByLabelText('search-input') as HTMLInputElement;
    return {
        input,
        ...utils,
    }
}
test('It should output the correct value', () => {
    const { input } = setup()
    fireEvent.change(input, {target: {value: 'al'}})
    expect(input.value).toBe('al')
});

test('It should hide dropdown when the search bar is out of focus', () => {
    const { input, container } = setup()
    fireEvent.click(input)

    fireEvent.blur(input);
    expect(container.getElementsByClassName('dropdown').length).toBe(0);
});


test('It should call search people api when the user starts to search', async () => {
    const { input } = setup()
    fireEvent.click(input)
    fireEvent.change(input, {target: {value: 'al pa'}})
    await waitFor(() => {
        expect(screen.getByText(/^Al Pacino$/i)).toBeTruthy();
    });
});

test('It should show filter tag when the search option is selected', async () => {
    const { input, container } = setup()
    fireEvent.click(input)
    fireEvent.change(input, {target: {value: 'al pa'}})
    await waitFor(() => {
        const option = screen.getByText(/^Al Pacino$/i);
        fireEvent.click(option);
    });

    fireEvent.click(input)
    fireEvent.change(input, {target: {value: 'al pa'}})
    await waitFor(() => {
        const option = screen.getByText(/^Alain Delon$/i);
        fireEvent.click(option);
    });

    expect(container.getElementsByClassName('tagItem').length).toBe(2)
    expect(container.getElementsByClassName('tagItem')[0].getElementsByClassName('title')[0].innerHTML).toBe('Al Pacino');
    expect(container.getElementsByClassName('tagItem')[1].getElementsByClassName('title')[0].innerHTML).toBe('Alain Delon');

    fireEvent.click(input)

    fireEvent.change(input, {target: {value: 'al pa'}})
    await waitFor(() => {
        expect(container.getElementsByClassName('warningMessage').length).toBe(1);
    });
});

test('It should remove filter tag when it is clicked', async () => {
    const { input, container } = setup()
    fireEvent.click(input)
    fireEvent.change(input, {target: {value: 'al pa'}})
    await waitFor(() => {
        const option = screen.getByText(/^Al Pacino$/i);
        fireEvent.click(option);
    });

    expect(container.getElementsByClassName('tagItem').length).toBe(1)

    const tagItem = container.getElementsByClassName('tagItem')[0];
    fireEvent.click(tagItem);

    expect(container.getElementsByClassName('tagItem').length).toBe(0)
    expect(container.getElementsByClassName('warningMessage').length).toBe(0)
});