import { getImagesUrl } from "../../../utils/utils";
import { AvatarIcon, CloseIcon } from "../../icons";
import styles from "../../../styles/modules/FilterTag.module.scss";

interface FilterTagProps {
    title: string,
    imageUrl: string,
    onSelectTag: () => void
}

const FilterTag = ({title, imageUrl, onSelectTag}: FilterTagProps) => {
    return (
        <div className={styles.tagItem} onClick={ () => onSelectTag()}>
            <div className={`${styles.image}`} style={imageUrl? {backgroundImage: `url(${getImagesUrl({width: 235, height: 235, profileUrl: imageUrl})})`}: {}}>
                {!imageUrl && <AvatarIcon/>}
            </div>
            <div className={styles.title}>{title}</div>
            <CloseIcon/>
        </div>
    )
}

export default FilterTag