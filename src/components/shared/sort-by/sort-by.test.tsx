import React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import SortBy from "./sort-by";

jest.useFakeTimers();
const setup = () => {
    const props = {
        getSelectedSortBy: (value: string) => {}
    }
    const utils = render(<SortBy {...props} />)
    return {
        ...utils
    }
}

test('It should render sort by component', () => {
    const { container } = setup()
    expect(container.getElementsByClassName('sortByContainer').length).toBe(1)
});

test('It should show the dropdown when the button is clicked', () => {
    const { container } = setup()
    const button = container.getElementsByClassName('sortByButton')[0];
    fireEvent.click(button);

    expect(container.getElementsByTagName('li').length).toBe(3);
});

test('It should change the sort by label after an option is selected', () => {
    const { container } = setup()
    const sortBybutton = container.getElementsByClassName('sortByButton')[0];
    fireEvent.click(sortBybutton);

    const option = container.getElementsByTagName('li')[1];
    fireEvent.click(option);

    expect(sortBybutton.getElementsByTagName('span')[0].innerHTML).toBe('Release date');
});

test('It should hide the dropdown when the button is out of focus', () => {
    const { container } = setup()
    const button = container.getElementsByClassName('sortByButton')[0];
    fireEvent.click(button);

    const dropdown = container.getElementsByClassName('sortByOption');
    expect(dropdown.length).toBe(1);

    fireEvent.blur(button);

    act(() => {
        jest.runAllTimers();
    })

    expect(dropdown.length).toBe(0);
})