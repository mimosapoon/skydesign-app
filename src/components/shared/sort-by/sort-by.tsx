import { memo, useState, MouseEvent } from "react";
import {ArrowIcon, TickIcon} from "../../icons";
import styles from '../../../styles/modules/SortBy.module.scss';


interface SortByProps{
    getSelectedSortBy: (value: string) => void
}

const SortBy = memo(({getSelectedSortBy}: SortByProps) => {
    const allOptions = [
        { label:'Rating', value:'vote_average'},
        { label:'Release date', value:'release_date'},
        { label:'Popularity', value:'popularity'}
    ];

    const [isActive, setIsActive] = useState(false);
    const [optionId, setOptionId] = useState(0);

    const onSelectedOption = (event: MouseEvent<HTMLLIElement>, selectedOptionId: number) => {
        event.stopPropagation();
        event.preventDefault()
        setOptionId(selectedOptionId);
        setIsActive(false);
        if( optionId === selectedOptionId) {
            return;
        }
        getSelectedSortBy(allOptions[selectedOptionId].value)

    }
    return (
        <div className={styles.sortByContainer}>
            <button
                className={`${styles.sortByButton} ${isActive ? styles.sortByButtonActive : ''}`}
                onClick={(event) => {
                    event.preventDefault()
                    setIsActive(!isActive)
                }}
                onBlur={ (event) => {
                    event.preventDefault();
                    event.stopPropagation();
                    setTimeout( () => setIsActive(false), 200);
                }}
            >
                <span>{ allOptions[optionId] ? allOptions[optionId].label : allOptions[0].label}</span>
                <i><ArrowIcon /></i>
            </button>
            { isActive &&
                <div className={styles.sortByOption}>
                    <div className={`${styles.title}`}>Sort by</div>
                    <ul>
                        {
                            allOptions.map( ({label, value}, index) => (
                                    <li key={`sortByOption-${index}`} onClick={ (event) => onSelectedOption(event, index)}>
                                        <span>{label}</span>
                                        { optionId === index ? <i><TickIcon /></i>: null }
                                    </li>
                                )
                            )
                        }
                    </ul>
                </div>
            }
        </div>
    )
})

export default SortBy