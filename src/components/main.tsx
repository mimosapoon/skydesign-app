import React, { useEffect, useState } from 'react';
import queryString from 'query-string';
import {ListingPage, ListingPageSkeleton, ThreeMain} from ".";
import { SearchBar, SortBy } from './shared';
import { WarningIcon, CarouselIcon, ListIcon } from "./icons";
import { searchCategories } from '../utils/constants';
import { discoverMovie } from '../api-utils/request';
import { DiscoverMovie } from '../api-utils/types/api';

const Main = () => {
    const maxSelectedOption = 2;
    const [selectedActors, setSelectedActors] = useState<string[]>([]);
    const [movieListings, setMovieListings ] = useState<DiscoverMovie | null>();
    const [movieListingsQueryObject, setMovieListingsQueryObject] = useState<{[key:string]: string | null}>();
    const [warningMessage, setWarningMessage] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState<boolean | null>(false);
    const [isCarouselView, setIsCarouselView] = useState<boolean>(false);

    useEffect( () => {
        setWarningMessage('')
        if(selectedActors.length === maxSelectedOption) {
            setMovieListingsQueryObject({
                with_people: selectedActors.join(','),
                sort_by: 'vote_average.desc'
            })
        }else {
            setMovieListings(null)
        }
    },[selectedActors]);

    useEffect( () => {
        if(movieListingsQueryObject) {
            const query = queryString.stringify(movieListingsQueryObject);
            setIsLoading(true);
            discoverMovie({queryString: query}).then( (data)=> {
                setWarningMessage('')
                setMovieListings(data);
                setIsLoading(false);
            }).catch( (err) => {
                setWarningMessage('Request failed, please try again')
                setIsLoading(false);
            })
        }
    }, [movieListingsQueryObject])

    return (
        <div className="main">
            <div className="content-top">
                <div className="panel container-fluid">
                    <div className="wrapper">
                        <h1>Sky design</h1>
                        <p>Input the names of two actors and displays a list of movies that feature both of those actors in starring roles.</p>
                        <div className="toolbar">
                            <SearchBar
                                placeHolder='Search any names'
                                searchType={searchCategories.PEOPLE.ACTING}
                                maxSelectedOption={maxSelectedOption}
                                getSelectedOptionIds={ (selectedOptions) => setSelectedActors(selectedOptions)}
                            />
                            {
                                movieListings && movieListings.results.length > 0 && (
                                    <>
                                        <button className="switch-view-button" onClick={ () => {
                                            setIsCarouselView(!isCarouselView)
                                        }}>
                                            {isCarouselView ?  <ListIcon />: <CarouselIcon/> }
                                        </button>
                                        <SortBy getSelectedSortBy={ (sortByValue) => {
                                            setMovieListingsQueryObject({
                                                ...movieListingsQueryObject,
                                                sort_by: `${sortByValue}.desc`
                                            })
                                        }
                                        }/>
                                    </>
                                )
                            }

                        </div>
                    </div>
                </div>
            </div>
            {
                isCarouselView ?
                    <div className="content-three">
                        { movieListings && ( movieListings.results.length > 0 && <ThreeMain listings={movieListings} /> )}
                    </div>
                    :
                    <div className="content-bottom">
                        <div className="panel container-fluid">
                            { isLoading
                                ? <ListingPageSkeleton count={2}/>
                                : <>
                                    { warningMessage && <div className="no-result"><WarningIcon/>{ warningMessage}</div> }
                                    {
                                        movieListings && ( movieListings.results.length > 0
                                            ? (
                                                <ListingPage listings={movieListings} updateQueryObject={ (newObject) => setMovieListingsQueryObject({
                                                    ...movieListingsQueryObject,
                                                    ...newObject
                                                })}/>
                                            )
                                            : (
                                                <div className="no-result"> 😥 Whoops, we couldn't find any results. </div>
                                            ))
                                    }
                                </>
                            }
                        </div>
                    </div>
            }
        </div>
    )
}

export default Main