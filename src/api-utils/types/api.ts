export interface SearchPeople {
    page: number,
    results: {
        adult: boolean,
        gender: number,
        id: string,
        known_for_department: string,
        name: string,
        original_name: string,
        popularity: number,
        profile_path: string,
        known_for?: string[]
    }[],
    total_pages: number,
    total_results: number,
    status_message?: string,
    status_code?: number
}

export interface DiscoverMovie {
    page: number,
    results: {
        adult: boolean,
        backdrop_path: null | string,
        genre_ids: number[],
        id: number,
        original_language: string,
        original_title: string,
        overview: string,
        popularity: number
        poster_path: string,
        release_date: string,
        title: string,
        video: boolean,
        vote_average: number,
        vote_count: number
    }[],
    total_pages: number,
    total_results: number
}

export interface FindPeopleDetails {
    id: string,
    also_known_as: string[],
    adult: boolean,
    biography: string,
    birthday: string,
    deathday: string,
    place_of_birth: string,
    popularity: string,
    profile_path: string,
    name: string
}