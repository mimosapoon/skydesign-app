import { DiscoverMovie, FindPeopleDetails, SearchPeople } from "./types/api";
import { getSearchCategory } from '../utils/utils';
import { searchCategories } from '../utils/constants';

const baseUrl = 'https://api.themoviedb.org/3';
const apiKey = '74861c76b16db29a7f02f0f50dd33ce5';
const language = 'en-US';
const includeAdult = false;

const requestItem = ({searchType, keyword}: {searchType: string, keyword: string}) => {
    const searchCategory = getSearchCategory({searchType, sourceObj: searchCategories});
    switch (searchCategory) {
        case 'PEOPLE':
            return searchPeople({keyword});

        // case 'MOVIE':
            // return searchMovie({keyword})
            // break;

        default:
            return searchPeople({keyword});
    }
}

const searchPeople = ({keyword}: {keyword: string}): Promise<SearchPeople> => {
    const url = `${baseUrl}/search/person?api_key=${apiKey}&language=${language}&query=${keyword}&includeAdult=${includeAdult}`;
    return request(url);
}

const discoverMovie = ({queryString}: {queryString: string}): Promise<DiscoverMovie> => {
    const url = `${baseUrl}/discover/movie?api_key=${apiKey}&language=${language}&${queryString}`;
    return request(url);
}

const findPeopleDetail = ( ({id}: {id: string}): Promise<FindPeopleDetails> => {
    const url = `${baseUrl}/person/${id}?api_key=${apiKey}&language=${language}`;
    return request(url);
})


const request =  (url:string, options = {}): Promise<any> => {
    return new Promise((resolve, reject) => {
        fetch(url, options)
            .then((res) => res.json())
            .then((data) => resolve(data))
            .catch((error)=> {
                console.log("catch?");
                reject(error)
            })
    })
}

export { searchPeople, findPeopleDetail, discoverMovie, requestItem }
