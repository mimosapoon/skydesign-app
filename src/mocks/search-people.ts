const searchPeopleMock = {
    page: 1,
    results: [
        {
            adult: false,
            gender: 0,
            id: '2738402',
            known_for_department: 'Sound',
            name: 'AL',
            original_name: 'AL',
            popularity: 0.6,
            profile_path: null,
            known_for: []
        },
        {
            adult: false,
            gender: 0,
            id: '2056472',
            known_for_department: 'Acting',
            name: 'Al',
            original_name: 'Al',
            popularity: 0.6,
            profile_path: null,
            known_for: []
        },
        {
            adult: false,
            gender: 2,
            id: '1273309',
            known_for_department: 'Acting',
            name: 'Alexander Dreymon',
            original_name: 'Alexander Dreymon',
            popularity: 160.275,
            profile_path: '/ifI2QhpUlAwiWNwdDsFMRlPovsk.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 2,
            id: '1795607',
            known_for_department: 'Acting',
            name: "Jahi Di'Allo Winston",
            original_name: "Jahi Di'Allo Winston",
            popularity: 9.744,
            profile_path: '/x13w2XYkCdjllsbrzyb1aKno27o.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '109513',
            known_for_department: 'Acting',
            name: 'Alexandra Daddario',
            original_name: 'Alexandra Daddario',
            popularity: 62.311,
            profile_path: '/lh5zibQXYH1MNqkuX8TmxyNYHhK.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 2,
            id: '64295',
            known_for_department: 'Acting',
            name: 'Alan Ritchson',
            original_name: 'Alan Ritchson',
            popularity: 31.464,
            profile_path: '/iV4XpUEsvnncbRv1SJKasRY05cK.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 2,
            id: '1158',
            known_for_department: 'Acting',
            name: 'Al Pacino',
            original_name: 'Al Pacino',
            popularity: 45.656,
            profile_path: '/fMDFeVf0pjopTJbyRSLFwNDm8Wr.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '56731',
            known_for_department: 'Acting',
            name: 'Jessica Alba',
            original_name: 'Jessica Alba',
            popularity: 19.29,
            profile_path: '/tyZ2r0tiPtTkE2udDEVbNXnMV4v.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 2,
            id: '15135',
            known_for_department: 'Acting',
            name: 'Alain Delon',
            original_name: 'Alain Delon',
            popularity: 23.762,
            profile_path: '/8HUyJPFr6wK7r7I4nkH37BVv8kc.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 2,
            id: '1903',
            known_for_department: 'Acting',
            name: 'Alan Arkin',
            original_name: 'Alan Arkin',
            popularity: 19.714,
            profile_path: '/aJUO84DmlxsUWLc4Vrx3NXUVMq4.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '88029',
            known_for_department: 'Acting',
            name: 'Alison Brie',
            original_name: 'Alison Brie',
            popularity: 27.791,
            profile_path: '/uu16GiwYblS6IJV3o4qFSLWKXOC.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '591835',
            known_for_department: 'Acting',
            name: 'Ali Wong',
            original_name: 'Ali Wong',
            popularity: 22.748,
            profile_path: '/uUtwpBkpxuLQFNVS3F5Bs4JnYOr.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '17303',
            known_for_department: 'Acting',
            name: 'Ali Larter',
            original_name: 'Ali Larter',
            popularity: 18.782,
            profile_path: '/e1LF7oh0C5jg30hdNS27a1pXPxU.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 2,
            id: '1629663',
            known_for_department: 'Acting',
            name: 'Alp Navruz',
            original_name: 'Alp Navruz',
            popularity: 16.58,
            profile_path: '/l1hxKyCzLbitYlve7HwIDfRAUqn.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '1357037',
            known_for_department: 'Acting',
            name: 'Alice Kremelberg',
            original_name: 'Alice Kremelberg',
            popularity: 26.257,
            profile_path: '/l82uQGCwzJ5AaotsoH7tRxIFsTR.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '650',
            known_for_department: 'Acting',
            name: 'Karen Allen',
            original_name: 'Karen Allen',
            popularity: 14.697,
            profile_path: '/eJszpndpRzrXbSlz7RUlApoTykn.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '64154',
            known_for_department: 'Acting',
            name: 'Sasha Alexander',
            original_name: 'Sasha Alexander',
            popularity: 18.785,
            profile_path: '/xHsZhwVA8hOKEVEMwoSanqoblBo.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '6279',
            known_for_department: 'Acting',
            name: 'Alexis Bledel',
            original_name: 'Alexis Bledel',
            popularity: 22.396,
            profile_path: '/mhkQzBtp2wuYoKhYqJ4IpDtbS3t.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '227454',
            known_for_department: 'Acting',
            name: 'Alicia Vikander',
            original_name: 'Alicia Vikander',
            popularity: 22.772,
            profile_path: '/9pmHTbXeRxUF51jJKthmHI49u9z.jpg',
            known_for: []
        },
        {
            adult: false,
            gender: 1,
            id: '1674769',
            known_for_department: 'Acting',
            name: 'Alba Baptista',
            original_name: 'Alba Baptista',
            popularity: 19.545,
            profile_path: '/gV5LYhN7BRc7xjySFek9gdm5tNA.jpg',
            known_for: []
        }
    ],
    total_pages: 500,
    total_results: 10000
}

export default searchPeopleMock;