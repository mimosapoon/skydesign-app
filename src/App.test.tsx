import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

jest.mock('query-string', () => ({}))

test('renders Header', () => {
  render(<App />);
  const header = screen.getByText(/Sky design/i);
  expect(header).toBeInTheDocument();
});