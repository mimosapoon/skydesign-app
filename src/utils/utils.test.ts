import { isLatinWords, getImagesUrl, filterSearchResultsByType } from './utils';

test('IsLatinWords should return correct output', () => {
    [
        {input: '123456', output: false},
        {input: '123asdd456', output: false},
        {input: 'al 12', output: false},
        {input: 'al pac', output: true},
    ].forEach( ({ input,output}) => {
        expect(isLatinWords(input)).toBe(output)
    })
});

test('GetImagesUrl should return correct output', () => {
    [
        {
            input: {width: 240, height: 250, profileUrl: '/test.jpg'},
            output: 'https://www.themoviedb.org/t/p/w240_and_h250_face/test.jpg'
        }
    ].forEach( ({input, output}) => {
        expect(getImagesUrl(input)).toBe(output)
    })
});

test('FilterSearchResultsByType should return correct output', () => {
    [
        {
            input: {
                type: 'Acting',
                results: [{
                    adult: false,
                    gender: 2,
                    id: '1273309',
                    known_for_department: 'Acting',
                    name: 'Alexander Dreymon',
                    original_name: 'Alexander Dreymon',
                    popularity: 160.275,
                    profile_path: '/ifI2QhpUlAwiWNwdDsFMRlPovsk.jpg',
                },{
                    adult: false,
                    gender: 2,
                    id: '123',
                    known_for_department: 'Music',
                    name: 'Music Man',
                    original_name: 'Music Man',
                    popularity: 12,
                    profile_path: '/test.jpg',
                }]
            },
            output: [{
                adult: false,
                gender: 2,
                id: '1273309',
                known_for_department: 'Acting',
                name: 'Alexander Dreymon',
                original_name: 'Alexander Dreymon',
                popularity: 160.275,
                profile_path: '/ifI2QhpUlAwiWNwdDsFMRlPovsk.jpg',
            }]
        }
    ].forEach( ({input, output}) => {
        expect(filterSearchResultsByType(input)).toStrictEqual(output)
    })
});