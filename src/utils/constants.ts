export interface NestedObjectProps {
    [key:string]: {
        [key:string]: string
    }
}

export const searchCategories = {
    PEOPLE: {
        ACTING: 'ACTING',
        SOUND: 'SOUND'
    },
    MOVIE: {
        TESTING: 'TESING'
    }
} as NestedObjectProps;