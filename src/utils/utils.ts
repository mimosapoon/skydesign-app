import { SearchPeople } from "../api-utils/types/api";
import { NestedObjectProps } from './constants'

const theMovieDbHost = 'https://www.themoviedb.org';
const localhost = '/api';

const isLatinWords = (source: string): boolean => /^[A-Za-z\s+]*$/.test(source);

const getImagesUrl= ({width, height, profileUrl, isLocal = false}: {width: number, height:number, profileUrl: string, isLocal?: boolean}) => `${isLocal ? localhost : theMovieDbHost}/t/p/w${width}_and_h${height}_face${profileUrl}`;

const filterSearchResultsByType = ({type, results}: {type: string, results: SearchPeople['results']}) => results.filter( ({known_for_department, gender}) => known_for_department && type && known_for_department.toUpperCase() === type.toUpperCase() && gender > 0)

const getSearchCategory = ({searchType, sourceObj}: {searchType: string, sourceObj: NestedObjectProps}) => {
    return Object.entries(sourceObj).reduce( (result, [key, value]) => {
        if(Object.keys(value).includes(searchType)) {
            result = key;
        }
        return result;
    }, '')
}


export { isLatinWords, getImagesUrl, getSearchCategory, filterSearchResultsByType }