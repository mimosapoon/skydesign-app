# Getting Started with Create React App

This project was initialised using Create React App and written in Typescript.

## Available Scripts

In the project directory, you can run:

### `npm install` or `yarn install` 
Install dependencies

### `npm start` or `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test` or `yarn test`

Launches the test runner in the interactive watch mode.\

### `npm test --coverage` or `yarn test --coverage`
It generates a report to show the overall test coverage for the code. \

### `npm run build` or `yarn build`

Builds the app for production to the `build` folder.\

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

## Features
#### Autocomplete on the actor search bar
To search for an actor, start by typing in the search bar. Autocomplete should be enabled when the input text is more than one character.

#### Filter tags
When an actor is selected from the autocomplete, a filter tag should be added that allows you to remove the selected actor.

#### Autocomplete disabled when two actors have been selected
As the app is to show the films that star both of any two given actors, when the second actor is selected, the autocomplete should be disabled even though you can see the search result. Just simply removing any of the filter tags will enable autocomplete.
To search actors, I am using the API provided by the movie database API v3. The endpoint is /search/person which is requested with a text query. 

#### Film lists Sorting and Pagination
The film lists can be sorted by popularity, release date and vote average (rating). The API endpoint is /discover/movie which is requested with the query strings including with_people, sort_by and page.

#### Loading Skeleton
After the film list request is sent and before the response is received, a loading skeleton should be displayed on the page.

#### Unit testing and Component testing
I use Jest and React testing library for testing React components. The code coverage for now is above 80%.

#### Express server
Add node express server to proxy images from https://image.tmdb.org and set cors for fixing cors issue. Run `yarn server` to start node server with port 8090

#### Threejs for 3d carousel
To enrich the user experience, user can switch between carousel and list views. I used React Three Fiber is used to render the 3D objects.

#### Production link
The link for the showcase: https://mimosapoon.com/projects/skyapp/ . Thank you for your time and enjoy the app!











