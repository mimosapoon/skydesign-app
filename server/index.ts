import express from 'express';
import cors from 'cors';
import { createProxyMiddleware } from 'http-proxy-middleware';

const PORT = process.env.PORT || 8090;


const proxyOptions = {
    target: 'https://image.tmdb.org',
    changeOrigin: true
}

const app = express();
app.set('x-powered-by', false);
app.use(cors());

app.use('/', createProxyMiddleware({
        ...proxyOptions,
        logLevel: 'debug'
    })
);

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});